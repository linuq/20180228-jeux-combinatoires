---
title: Jeux combinatoires
author: François Pelletier
date: 28 février 2018
---

## Optimisation

Branche des mathématiques

- Modéliser, analyser, résoudre analytiquement.
- Problèmes qui ont un objectif qu'on peut définir par une fonction mathématique.
- Domaines
    - Recherche opérationnelle
    - Théorie des jeux
    - Théorie du contrôle

## Types d'optimisation

- Optimisation numérique: on cherche un minimum ou un maximum d'une fonction réelle (le nombre de valeurs possibles est infini)
- Optimisation combinatoire: on recherche les bonnes solutions parmi toutes les combinaisons possibles (le nombre de solutions possible est fini, même si il peut être très grand)
- Outil mathématique utilisé: Algorithmes

## Optimisation combinatoire

- Il existe plusieurs domaines concernés par l'optimisation combinatoires:
    - Programmation linéaire
    - Optimisation avec contraintes
    - Problèmes avec nombres entiers
    - Programmation dynamique (décomposer en sous-problèmes)
- Ces domaines sont connexes et se recoupent souvent

## Optimisation avec contraintes

- Les contraintes de ce type de problèmes sont les "règles du jeu":
    - Dans la construction d'un horaire de travail, ce sont le nombre d'employés et les conditions de travail
    - Dans la planification d'un travail, c'est les ressources disponibles, l'ordre des tâches et la quantité de travail à faire
    - Dans un jeu, ce sont les différents réglements
- Nous allons nous intéresser aux jeux

## Jeux combinatoires

- Ce sont souvent des problèmes de type "casse-tête" qui ont peu de solutions.
- Il peut être très long de tenter de trouver les solutions en essayant les combinaisons une par une
- Il faut développer des stratégies pour éliminer des ensembles de solutions.
- On peut choisir deux modes de résolution: trouver une solution valide ou trouver la meilleure solution. Dans ce cas, il faut définir une fonction mathématique qui sert à mesurer la qualité d'une solution.

## Quels logiciels existent pour nous aider ?

- Les logiciels permettant de résoudre de tels jeux sont appelés solveurs.
- En général, ce sont des librairies de fonctions qu'on integre dans un programme. Il en existe pour la plupart des langages de programmation populaires.
- Nous en utiliserons deux:
    - OR Tools de Google (Python, C++)
    - OscaR (Scala)
- Ces logiciels sont similaires, ils proposent:
    - Un outil de construction de modèles
    - Un catalogue de contraintes
    - Des heuristiques pour accélérer la résolution
    - Un outil d'optimisation

## Les jeux à résoudre.

- Il existe d'innombrables problèmes ou jeux pouvant être résolus.
- En voici quelques uns:
    - Sudoku
    - Échecs (n-reines)
    - Mots entrercroisés
    - Kakuro: un mélange du sudoku et des mots entrecroisés
    - Coloriage de cartes
    - Configuration de feux de circulation avec piétons
    - Création de menus équilibrés
